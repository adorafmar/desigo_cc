
// testing GIT install on Desigo machine
process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 0;

require('dotenv').config();
const moment = require('moment');
const axios = require('axios');
const fs = require('fs');
let timer0;
// let part_of_day_previous = (new Date).getHours();
let collection = [];
let tags = []

async function get_data(){
	const time_stamp = moment().format("YY.MM.DD_HH.mm.ss");
	console.info(`${time_stamp} acquisition begins`);
	console.error(`${time_stamp} acquisition begins`);
	let authorization_error = false;
	let token;
	let data;
	var collection = [];
	const credentials = `grant_type=password&username=${process.env.DB_USER}&password=${process.env.DB_PASS}`;
	console.info(`${time_stamp} requesting authorization`);
	await axios.post('https://fac-siemens.austin.utexas.edu/WSI/api/token', credentials)
	  .then(res => {
			console.info(`${time_stamp} received token`);   
	  	token = res.data.access_token
	  })
	  .catch(err => {
	  	console.error(`${time_stamp} ` + err);
	  	authorization_error = true;
			collection.push(`${time_stamp} ` + JSON.stringify(err));
  });
	if(authorization_error){
		console.error(`${time_stamp} authorization_error`);
		collection.push("authorization_error");
	} else {
		let tags_togo = [].concat(tags);
		while (tags_togo.length != 0){
			const tag = tags_togo.pop();
			console.info(`${time_stamp} requesting ${tag}`);
			console.info(`${time_stamp} tags_togo ${tags_togo.length}`);
			// https://fac-siemens.austin.utexas.edu/WSI/api/values/System1:GMS_AP2_GWBˌAHU01ˌCCT 
			await axios.get(encodeURI(`https://fac-siemens.austin.utexas.edu/WSI/api/values/System1:${tag}`),
				{ headers: {"Authorization" : "Bearer " + token}})
				.then(res => {
					if(typeof res.data != 'object'){
						console.error(`${time_stamp} NO DATA for ${tag}`);
						// tags_togo = []; // used to stop loop and terminate after finding an unresolvable tag
					} else {
						console.info(JSON.stringify(res.data, null, 1));
						collection.push(res.data);
					}
				})
				.catch(err => {
			    console.error(`${time_stamp} error: ` + err);
			});
		}
	}
	let file_name = `C:\\Users\\Public\\Documents\\DesigoCC\\desigo_tags_${time_stamp}.json`;
	console.info(`${time_stamp} writting to ` + file_name);
	fs.writeFile(file_name, JSON.stringify(collection, null, 4), (err) => {
	    if (err) {
				console.error(`${time_stamp} writeFile error: ${err}`); 
	    } else {
				console.info(`${time_stamp} data is saved to ` + file_name);
	    }
		console.info(`${time_stamp} acquisition ends`);
		console.error(`${time_stamp} acquisition ends`);
		setTimeout(get_data, 420000);
	});
}
try {
  tags = fs.readFileSync('tags.txt', 'utf8').replace(/\r\n/g,'\n').split('\n').reverse();
} catch (err) {
  console.error(err)
}
get_data();