const fs = require("fs");
const execSync = require('child_process').execSync;

function send_to_storj(){
	console.log("sending started at " + Date())
	fs.readdir("Z:\\", function (err, files) {
	    if (err) {
	        return console.log('Unable to scan directory: ' + err);
	    } 
	    file_to_send = files.sort().reverse()[0];
		console.log(file_to_send);
		let output; 
		output = execSync(`copy z:\\${file_to_send} .`, { encoding: 'utf-8' });
		// C:\uem\desigo_cc>C:\cygwin64\bin\truncate -s 4000 desigo_tags_22.03.15_13.27.44.json
		output = execSync(`C:\\cygwin64\\bin\\truncate -s 4000 ${file_to_send}`, { encoding: 'utf-8' }); 
		// C:\Users\iamtradmin\uplink cp desigo_tags_22.03.15_13.23.44.json sj://fdd
		output = execSync(`C:\\Users\\iamtradmin\\uplink cp ${file_to_send} sj://fdd`, { encoding: 'utf-8' });  
		output = execSync(`del ${file_to_send}`, { encoding: 'utf-8' }); 
	});
}
send_to_storj();
// setInterval(send_to_storj,900000); using system scheduler (3rd party app)