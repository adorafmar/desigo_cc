process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 0;

require('dotenv').config();
const moment = require('moment');
const axios = require('axios');
const fs = require('fs');
let timer0;

async function get_data(){
	const time_stamp = moment().format("YY.MM.DD_HH.mm.ss");
	let token;
	let data;
	const credentials = `grant_type=password&username=${process.env.DB_USER}&password=${process.env.DB_PASS}`;
	console.log(`${time_stamp} requesting authorization`);
	await axios.post('https://fac-siemens.austin.utexas.edu/WSI/api/token', credentials)
	    .then(res => token = res.data.access_token)
	    .catch(err => {
	    	console.log(`${time_stamp} error: ` + err);
	    	data = err;
	    }); 
	console.log(`${time_stamp} requesting data`);     
	await axios.get(encodeURI('https://fac-siemens.austin.utexas.edu/WSI/api/values/System1:GMS_AP2_GWBˌAHU01ˌSAT'),
		{ headers: {"Authorization" : "Bearer " + token } })
		.then(res => data = res.data)
		.catch(err => {
	    	console.log(`${time_stamp} error: ` + err);
	    	data = err;
	    }); 
	data = JSON.stringify(data);
	let file_name = `C:\\Users\\Public\\Documents\\DesigoCC\\desigo_data_${time_stamp}.json`;
	console.log(`${time_stamp} writting to ` + file_name);
	fs.writeFile(file_name, data, (err) => {
	    if (err) {
	        throw err;
	    }
		console.log(`${time_stamp} data is saved to ` + file_name);
	});
	file_name = `C:\\Users\\Public\\Documents\\DesigoCC\\desigo_data.json`;
	console.log(`${time_stamp} writting to ` + file_name);
	fs.writeFile(file_name, data, (err) => {
	    if (err) {
	        throw err;
	    }
		console.log(`${time_stamp} data is saved to ` + file_name);
	});
}
get_data();
clearTimeout(timer0);
timer0 = setInterval(get_data, 300000);