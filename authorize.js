process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 0;
const axios = require('axios');

const data = 'grant_type=password&username=aestrada&password=1234';
axios.post('https://fac-siemens.austin.utexas.edu/WSI/api/token', data)
    .then((res) => {
        console.log(`Status: ${res.status}`);
        console.log('Body: ', res.data);
    }).catch((err) => {
        console.error(err);
    });